// Creating an Object using Object Literal Notation
const person = {
    name: 'John Doe',
    age: 30,
    profession: 'Engineer',
    greet: function() {
      console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
    }
  };
  
  console.log(person.name); // Output: John Doe
  console.log(person['age']); // Output: 30
  person.greet(); // Output: Hello, my name is John Doe and I am 30 years old.


  // USing the constructor function to create 'Person' Objects
/*  function Person(name, age){
    this.name = name;
    this.age = age;

    this.greet = function(){
      console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`)
    }
  }

  // Creating Instances of 'Person' using the 'new' keyword

  const person1 = new Person('John Doe', 30);
  const person2 = new Person('Jane Smith', 25);

  console.log(person1.name)
  console.log(person2['age'])
  person1.greet();*/

/* 
Constructors provide a way to create multiple instances of an object with shared properties and methods. They allow for the creation of similar objects without duplicating code.

Object literals are useful for creating single instances of an object with specific properties and values.
*/
/*
  Classes are a more modern approach introduced in ECMAScript 2015 (ES6) for creating objects in JavaScript.
*/

  // Class to create 'Person' objects
  class Person2{
    constructor(name, age){
      this.name = name;
      this.age = age;
    }
    greet(){
      console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`)
    }
  }

  // Creating Instances of 'Person' using class 

  const personA = new Person2('John Doe', 30)
  const personB = new Person2('Jane Smith', 25)

   console.log(personA.name);
   console.log(personB.age);
   personA.greet();
  

class Rectangle{
  constructor(width, height){
    this.width = width;
    this.height = height;

  }

  getArea(){
    return this.width * this.height;
  }

  getPerimeter(){
    return 2*(this.width + this.height)
  }
}

const rectangle1 = new Rectangle(5, 10);

console.log(rectangle1.getArea());
console.log(rectangle1.getPerimeter());


// Object Literal
const car1 = {
  brand: 'Toyota',
  model: 'Camry',
  year: 2021,
  startEngine() {
    console.log(`Starting the engine of ${this.brand} ${this.model}`);
  },
};

car1.startEngine();

// Constructor function
function Car(brand, model, year) {
  this.brand = brand;
  this.model = model;
  this.year = year;
}

Car.prototype.startEngine = function(){
  console.log(`Starting the engine of ${this.brand} ${this.model}`);
}

const car2 = new Car('Honda', 'Civic', 2022)
car2.startEngine();

// More on Prototypes and Inheritance discussion
/* 
Understanding of prototypes, prototype chains, and how to implement inheritance in JavaScript using both constructor functions and classes. 

Create objects with shared behavior, extend functionality, and build hierarchical relationships between objects.
*/

// Step 1: Creating a prototype Object using Object literals
/*
 in this step, we create a personPrototype object using object literals. The personPrototype serves as a blueprint or template for creating objects with shared behavior. It contains a single method introduce(), which will be shared among all objects created from this prototype.
*/

const personPrototype = {
  introduce(){
    console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
  }
}

// Step 2: Creating an object using prototype object
/*
  Here, we create an object personX using the Object.create() method. By passing in personPrototype as the argument, we establish a prototype link between person1 and personPrototype.
  We then assign specific values to the name and age properties of person1 and invoke the introduce() method. Since personX doesn't have its own introduce() method, it looks up the prototype chain and finds the method in personPrototype.
*/
const personX = Object.create(personPrototype)
personX.name = 'John Doe';
personX.age = 29.9;
personX.introduce();

// Step 3: Creating a constructor function and adding methods to the prototype
/*
  In this step, we define a constructor function Person. Constructors are used to create objects with specific properties and behaviors. Inside the Person constructor, we assign the name and age values to the newly created object using the this keyword. We then add the introduce() method to the prototype of the Person constructor using Person.prototype. By doing this, all objects created from the Person constructor will share the same introduce() method.
*/

function Person(name, age){
  this.name = name;
  this.age = age;
}

Person.prototype.introduce = function(){
  console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
}

// Step 4: Creating an object using constructor function
const person2A = new Person('Jane Smith', 25);
person2A.introduce();


// Step 5: Creating a child object with a specific prototype
/*
  In this step, we create an employeePrototype object using Object.create() and set its prototype to personPrototype. This establishes a prototype chain where employeePrototype inherits from personPrototype. We also add a jobTitle property specific to employees to the employeePrototype. Then, we create an employee1 object using Object.create() and assign employeePrototype as its prototype. We provide the name, age, and jobTitle values to employee1 and invoke the introduce() method. Since employee1

  Since employee1 doesn't have its own introduce() method, it traverses the prototype chain and finds the method in personPrototype.
*/

const employeePrototype = Object.create(personPrototype)
employeePrototype.jobTitle = '';

const employee1 = Object.create(employeePrototype)
employee1.name = 'Michael Johnson';
employee1.age = 27;
employee1.jobTitle = 'Software Engineer';
employee1.introduce();

// Step 6: Inheriting from a parent constructor using extends keyword
/*
  In this step, we define a child class Employee that extends the parent class Person using the extends keyword. The extends keyword allows the child class to inherit properties and methods from the parent class. Inside the child class's constructor, we call super(name, age) to invoke the parent constructor and set the name and age values. We also add a jobTitle property specific to employees.
*/

class Employee extends Person2{
  constructor(name, age, jobTitle){
    super(name, age)
    this.jobTitle = jobTitle;

  }

  introduce() {
      console.log(`Hello, my name is ${this.name}, I am ${this.age} years old, and I work as a ${this.jobTitle}.`);
    }

}

const employee2 = new Employee('Sarah Davis', 28, 'Project Manager')
employee2.introduce();
