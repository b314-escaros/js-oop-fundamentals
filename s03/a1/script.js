// PART 1
// Student class
class Student {
  // to enable students instantiated from this class to have distinct names and emails
  // our constructor must be able to accept name and email arguments
  // which it will then use to set the value of the object's corresponding properties
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.grades = grades;
  }

  // class methods
  // these are common to all instances
  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout(){
  	console.log(`${this.email} has logged out`);
    return this;
  }
   listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
      return this;
    }
   computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        this.gradeAve = sum / 4;
        return this;
      }
   willPass() {
    this.passed = this.computeAve().gradeAve >= 85;
    return this;
  	}

   willPassWithHonors() {
  	    if (this.passed) {
  	      this.passedWithHonors = this.gradeAve >= 90;
  	    } else {
  	      this.passedWithHonors = false;
  	    }
  	    return this;
  	  }
  }

  let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
  let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
  let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
  let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

  // studentOne
  studentOne.login(); 
  studentOne.logout(); 

  studentOne.listGrades(); 

  studentOne.computeAve();
  console.log(studentOne.gradeAve); 

  studentOne.willPass();
  console.log(studentOne.passed); 

  studentOne.willPassWithHonors();
  console.log(studentOne.passedWithHonors); 

  // studentTwo
  studentTwo.login();
  studentTwo.logout();

  studentTwo.listGrades(); 

  studentTwo.computeAve();
  console.log(studentTwo.gradeAve); 

  studentTwo.willPass();
  console.log(studentTwo.passed); 

  studentTwo.willPassWithHonors();
  console.log(studentTwo.passedWithHonors); 

  // studentThree
  studentThree.login();
  studentThree.logout();

  studentThree.listGrades(); 

  studentThree.computeAve();
  console.log(studentThree.gradeAve); 

  studentThree.willPass();
  console.log(studentThree.passed); 

  studentThree.willPassWithHonors();
  console.log(studentThree.passedWithHonors); 

  // studentFour
  studentFour.login(); 
  studentFour.logout(); 

  studentFour.listGrades(); 

  studentFour.computeAve();
  console.log(studentFour.gradeAve); 

  studentFour.willPass();
  console.log(studentFour.passed); 

  studentFour.willPassWithHonors();
  console.log(studentFour.passedWithHonors); 



// PART 2
  /*
	1) What is the blueprint where objects are created from?
	Answer: class
	2) What is the naming convention applied to classes?
	Answer: PascalCase
	3) What keyword do we use to create objects from a class?
	Answer: 'new'
	4) What is the technical term for creating an object from a class?
	Answer: instantiation
	5) What class method dictates HOW objects will be created from that class?
	Answer: Constructor method
  */