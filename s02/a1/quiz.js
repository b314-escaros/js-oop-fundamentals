// PART 1
/*
1. What is the term given to unorganized code that's very hard to work with?
"spaghetti code"

2. How are object literals written in JS?
using curly braces {}

3. What do you call the concept of organizing information and functionality to belong to an object?
"object-oriented programming" (OOP).

4. If studentOne has a method named enroll(), how would you invoke it?
studentOne.enroll()

5. True or False: Objects can have objects as properties.
True. Objects can have objects as properties.

6. What is the syntax in creating key-value pairs?
 key: value, where the key is a string (or a symbol in ES6+) and the value can be any valid JavaScript expression.

7. True or False: A method can have no parameters and still work.
True. A method can have no parameters and still work.

8. True or False: Arrays can have objects as elements.
True. Arrays can have objects as elements.

9. True or False: Arrays are objects.
False. Arrays are not objects.

10. True or False: Objects can have arrays as properties.
True. Objects can have arrays as properties. 

*/

