// PART 2
//1. Translate the other students from our boilerplate code into their own respective objects.

let studentTwo = {
  name: 'Joe',
  email: 'joe@mail.com',
  grades: [78, 82, 79, 85],
  computeAverage: computeAverage
};

let studentThree = {
  name: 'Jane',
  email: 'jane@mail.com',
  grades: [87, 89, 91, 93],
  computeAverage: computeAverage
};

let studentFour = {
  name: 'Jessie',
  email: 'jessie@mail.com',
  grades: [91, 89, 92, 93],
  computeAverage: computeAverage
};


//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
// Method for studentOne
function computeAverage() {
  let sum = this.grades.reduce((total, grade) => total + grade, 0);
  return sum / this.grades.length;
}


//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.
studentTwo.willPass = function() {
  return this.computeAverage() >= 85;
};

studentThree.willPass = function() {
  return this.computeAverage() >= 85;
};

studentFour.willPass = function() {
  return this.computeAverage() >= 85;
};

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
studentTwo.willPassWithHonors = function() {
  let average = this.computeAverage();
  if (average >= 90) {
    return true;
  } else if (average >= 85) {
    return false;
  } else {
    return undefined;
  }
};

studentThree.willPassWithHonors = function() {
  let average = this.computeAverage();
  if (average >= 90) {
    return true;
  } else if (average >= 85) {
    return false;
  } else {
    return undefined;
  }
};

studentFour.willPassWithHonors = function() {
  let average = this.computeAverage();
  if (average >= 90) {
    return true;
  } else if (average >= 85) {
    return false;
  } else {
    return undefined;
  }
};

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
let classOf1A = {
  students: [studentTwo, studentThree, studentFour],

  //6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
  countHonorStudents: function() {
    let count = 0;
    this.students.forEach(function(student) {
      if (student.willPassWithHonors() === true) {
        count++;
      }
    });
    return count;
  },

  //7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
  honorsPercentage: function() {
    let honorCount = this.countHonorStudents();
    return (honorCount / this.students.length) * 100;
  },

  //8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
  retrieveHonorStudentInfo: function() {
    let honorStudentsInfo = [];
    this.students.forEach(function(student) {
      if (student.willPassWithHonors() === true) {
        honorStudentsInfo.push({ email: student.email, averageGrade: student.computeAverage() });
      }
    });
    return honorStudentsInfo;
  },

  //9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
  sortHonorStudentsByGradeDesc: function() {
    let honorStudentsInfo = this.retrieveHonorStudentInfo();
    return honorStudentsInfo.sort(function(a, b) {
      return b.averageGrade - a.averageGrade;
    });
  },
};
