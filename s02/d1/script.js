// use an object literals: {} to create an object representing a user
// encapsulation- whenever we add properties or methods to an object, we are performing ENCAPSULATION
// the organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them


// Create student objects
let studentOne = {
name: 'John',
email: 'john@mail.com',
grades: [89, 84, 78, 88],
// methods
	// add the functionalities available to a student as object methods
	// "this" keyword refers to the object encapsulating the method where "this" is called

	login(){
		console.log(`${this.email} has logged in`)
	},

	logout(){
		console.log(`${this.email} has logged out`)
	},

	listGrades(){
		console.log(`${this.name}'s quarterly grade average are ${this.grades}`)
	}

};

// log the content of studentOne's encapsulation information in the console
console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grade average is ${studentOne.grades}`);
console.log(studentOne)


// Mini-activity 1: Create a function that will get the quarterly average of studentOne's grades

function getQuarterlyAverage(student) {
const sum = student.grades.reduce((total, grade) => total + grade, 0);
const average = sum / student.grades.length;
return average;
}

// console.log(getQuarterlyAverage(studentOne)); 

// Mini-activity 2: Create a function that will return true if the average grade is >= 85, false otherwise

function isPassingGrade(student) {
const average = getQuarterlyAverage(student);
return average >= 85;
}

// console.log(isPassingGrade(studentOne)); 

// Mini-activity 3: Create a function called willPassWithHonors() that returns true if the student has passed and their average is >= 90. The function returns false if either one is not met.

function willPassWithHonors(student) {
const average = getQuarterlyAverage(student);
return isPassingGrade(student) && average >= 90;
}

// console.log(willPassWithHonors(studentOne)); 







/*let studentTwo = {
name: 'Joe',
email: 'joe@mail.com',
grades: [78, 82, 79, 85],
};

let studentThree = {
name: 'Jane',
email: 'jane@mail.com',
grades: [87, 89, 91, 93],
};

let studentFour = {
name: 'Jessie',
email: 'jessie@mail.com',
grades: [91, 89, 92, 93],
};

// Actions that students may perform
function login(student) {
console.log(${student.email} has logged in);
}

function logout(student) {
console.log(${student.email} has logged out);
}

function listGrades(student) {
student.grades.forEach(grade => {
console.log(grade);
});
}*/