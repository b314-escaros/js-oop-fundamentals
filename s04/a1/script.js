/*
Student Class
*/

class Student {
    constructor(name, email, grades){
        this.name = name;
        this.email = email;
        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
    }

    login(){
        console.log(`${this.email} has logged in`);
        return this;
    }
    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum/4;
        return this;
    }
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }
    willPassWithHonors() {
        if (this.passed) {
            if (this.gradeAve >= 90) {
                this.passedWithHonors = true;
            } else {
                this.passedWithHonors = false;
            }
        } else {
            this.passedWithHonors = false;
        }
        return this;
    }
}


class Section {
    constructor(name){
        // Initialize the Section object with a name and an empty array of students.
        this.name = name;
        this.students = [];
        // Initialize honorStudents and honorsPercentage as undefined.
        this.honorStudents = undefined;
        this.honorsPercentage = undefined;
    }

    addStudent(name, email, grades){
        // Method for adding a student to this section.
        // Create a new Student object with the provided details and grades,
        // and add it to the students array.
        this.students.push(new Student(name, email, grades));
        // Return the Section object itself to allow method chaining.
        return this;
    }

    countHonorStudents(){
        // Method for computing the number of honor students in the section.
        let count = 0;
        // Iterate through each student in the students array.
        this.students.forEach(student => {
            // Check if the student passed with honors.
            if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
                // If the student passed with honors, increment the count.
                count++;
            }
        })
        // Update the honorStudents property with the final count.
        this.honorStudents = count;
        // Return the Section object itself to allow method chaining.
        return this;
    }

    computeHonorsPercentage(){
        // Method for computing the percentage of honor students in the section.
        // Calculate the honors percentage based on the honorStudents count and the total number of students.
        this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
        // Return the Section object itself to allow method chaining.
        return this;
    }
}


/*

1) Create a new instance of the Section class named section1A
2) Add students to the section1A using the addStudent method:


  2.1) Add a student named John with email 'john@mail.com' and grades [89, 84, 78, 88]:

  2.2)Add a student named Joe with email 'joe@mail.com' and grades [78, 82, 79, 85]:

  2.3) Add a student named Jane with email 'jane@mail.com' and grades [87, 89, 91, 93]:

  2.4)Add a student named Jessie with email 'jessie@mail.com' and grades [91, 89, 92, 93]:


3. Count the number of honor students in the section using the countHonorStudents method:

4. Compute the percentage of honor students in the section using the computeHonorsPercentage method:

5. Display the results:
  5.1)Print the number of honor students:
  5.2) Print the percentage of honor students:


*/

// 1) Create a new instance of the Section class named section1A
const section1A = new Section('section1A');

// 2) Add students to the section1A using the addStudent method:
section1A
  .addStudent('John', 'john@mail.com', [89, 84, 78, 88])
  .addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85])
  .addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93])
  .addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

// 3) Count the number of honor students in the section using the countHonorStudents method:
section1A.countHonorStudents();

// 4) Compute the percentage of honor students in the section using the computeHonorsPercentage method:
section1A.computeHonorsPercentage();

// 5) Display the results:
console.log('Number of honor students:', section1A.honorStudents);
console.log('Percentage of honor students:', section1A.honorsPercentage + '%');
