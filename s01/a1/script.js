// Part 1
/*
1) How do you create arrays in JS? 
Answer: you can create arrays using square brackets [] or the Array constructor.
2) How do you access the first character of an array? 
Answer: using square brackets [] index 0, arrayName[0] 
3) How do you access the last character of an array? 
Answer: using square brackets [] index -1, arrayName[-1] 
4) What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array. 
Answer: indexOf() method
5) What array method loops over all elements of an array, performing a user-defined function on each iteration?
Answer: forEach() method
6) What array method creates a new array with elements obtained from a user-defined function?
Answer: map() method
7) What array method checks if all its elements satisfy a given condition?
Answer: every() method
8) What array method checks if at least one of its elements satisfies a given condition?
Answer: some() method
9) True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
Answer: False. The array.splice() method modifies the original array by adding or removing elements. It does not create a copy of the array. 
10) True or False: array.slice() copies elements from original array and returns them as a new array
Answer: False. The array.slice() method returns a new array containing a copy of the selected elements from the original array

*/


// Part 2
/*Function coding

1) Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
*/


// Answer: 
function addToEnd(arr, str) {
  if (typeof str !== 'string') {
    return 'error - can only add strings to an array';
  }

  arr.push(str);
  return arr;
}

// Testing the function
const students = ["john", "joe", "jane", "jessie"];
let updatedArray = addToEnd(students, 'Ryan');
console.log(updatedArray);

/* 
Output
addToEnd(students, "Ryan"); //["John", "Joe", "Jane", "Jessie", "Ryan"]
addToEnd(students, 045); //"error - can only add strings to an array"
*/



/* 
2) Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing. 
*/

// Answer:
function addToStart(arr, str) {
  if (typeof str !== 'string') {
    return 'error - can only add strings to an array';
  }

  arr.unshift(str);
  return arr;
}

// Testing the function
// let students2 = ['John', 'Joe', 'Jane', 'Jessie', 'Ryan'];
let updatedArray2 = addToStart(students, 'Tess');
console.log(updatedArray);


/*Output
addToStart(students, "Tess"); //["Tess", "John", "Joe", "Jane", "Jessie", "Ryan"]
validation check
addToStart(students, 033); //"error - can only add strings to an array"
*/


/*
3) Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.*/


// Answer:
function elementChecker(arr, value) {
  if (arr.length === 0) {
    return 'error - passed in array is empty';
  }

  return arr.includes(value);
}

// Testing the function
// let students = ['John', 'Joe', 'Jane', 'Jessie', 'Ryan'];
console.log(elementChecker(students, 'Jane')); // true
console.log(elementChecker([], 'Jane')); // 'error - passed in array is empty'



/*4) Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:*/

// if array is empty, return "error - array must NOT be empty"
// if at least one array element is NOT a string, return "error - all array elements must be strings"
// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
// if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
// if every element in the array ends in the passed in character, return true. Otherwise return false.

// Use the students array and the character "e" as arguments when testing.


// Answer:
function checkAllStringsEnding(arr, character) {
  if (arr.length === 0) {
    return 'error - array must NOT be empty';
  }

  if (!arr.every(element => typeof element === 'string')) {
    return 'error - all array elements must be strings';
  }

  if (typeof character !== 'string') {
    return 'error - 2nd argument must be of data type string';
  }

  if (character.length !== 1) {
    return 'error - 2nd argument must be a single character';
  }

  return arr.every(element => element.endsWith(character));
}

// Testing the function
// let students = ['John', 'Joe', 'Jane', 'Jessie', 'Ryan'];
console.log(checkAllStringsEnding(students, 'e')); // false
console.log(checkAllStringsEnding([], 'e')); // 'error - array must NOT be empty'
console.log(checkAllStringsEnding(['Jane', 2], 'e')); // 'error - all array elements must be strings'
console.log(checkAllStringsEnding(students, 4)); // 'error - 2nd argument must be of data type string'
console.log(checkAllStringsEnding(students, 'el')); // 'error - 2nd argument must be a single character'


/*
5) Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.*/

function stringLengthSorter(arr) {
  if (!arr.every(element => typeof element === 'string')) {
    return 'error - all array elements must be strings';
  }

  return arr.sort((a, b) => a.length - b.length);
}

// Testing the function

console.log(stringLengthSorter(students)); // ["Joe", "Tess", "John", "Jane", "Ryan", "Jessie"]
console.log(stringLengthSorter([037, 'John', 039, 'Jane'])); // 'error - all array elements must be strings'


/*
6) Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:
*/


// Answer:
function startsWithCounter(arr, character) {
  if (arr.length === 0) {
    return 'error - array must NOT be empty';
  }

  if (!arr.every(element => typeof element === 'string')) {
    return 'error - all array elements must be strings';
  }

  if (typeof character !== 'string') {
    return 'error - 2nd argument must be of data type string';
  }

  if (character.length !== 1) {
    return 'error - 2nd argument must be a single character';
  }

  const regex = new RegExp(`^${character}`, 'i');
  return arr.filter(element => regex.test(element)).length;
}

// Testing the function
console.log(startsWithCounter(students, 'J'));

/*
7) Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
*/
// if array is empty, return "error - array must NOT be empty"
// if at least one array element is NOT a string, return "error - all array elements must be strings"
// if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
// return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

// Use the students array and the string "jo" as arguments when testing.


// Answer:
function likeFinder(arr, searchString) {
  if (arr.length === 0) {
    return 'error - array must NOT be empty';
  }

  if (!arr.every(element => typeof element === 'string')) {
    return 'error - all array elements must be strings';
  }

  if (typeof searchString !== 'string') {
    return 'error - 2nd argument must be of data type string';
  }

  const regex = new RegExp(searchString, 'i');
  return arr.filter(element => regex.test(element));
}

// Testing the function
console.log(likeFinder(students, 'jo')); // ["Joe", "John"]



/*
8) Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.*/

// Answer:
function randomPicker(arr) {
  if (arr.length === 0) {
    return 'error - array must NOT be empty';
  }

  const randomIndex = Math.floor(Math.random() * arr.length);
  return arr[randomIndex];
}

// Testing the function
console.log(randomPicker(students));

